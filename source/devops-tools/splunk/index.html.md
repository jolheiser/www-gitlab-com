---
layout: markdown_page
title: "Splunk"
---
<!-- This is the MVC template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line

## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line

## Pricing
   - summary, links to tool website  <-- comment. delete this line

## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Splunk is an American multinational corporation based in San Francisco, California, which produces software for searching, monitoring, and analyzing machine-generated big data, via a web-style interface. Splunk (the product) captures, indexes and correlates real-time data in a searchable repository from which it can generate graphs, reports, alerts, dashboards and visualizations.


## Resources
* [Splunk Website](https://www.splunk.com/)
* [Wikipedia](https://en.wikipedia.org/wiki/Splunk)

## Pricing
- [Splunk Pricing](https://www.splunk.com/en_us/software/pricing.html)
  * [Splunk Enterprise Pricing FAQ](https://www.splunk.com/en_us/software/pricing/faqs.html#Splunk-Enterprise)
  * [Splunk Cloud Pricing FAQ](https://www.splunk.com/en_us/software/pricing/faqs.html#Splunk-Cloud)
  * [Splunk Light Software Pricing FAQ](https://www.splunk.com/en_us/software/pricing/faqs.html#Splunk-Light)
  * [Splunk Light Cloud Pricing FAQ](https://www.splunk.com/en_us/software/pricing/faqs.html#Splunk-LightCloud)

## Related Tools

* [Datadog](https://about.gitlab.com/devops-tools/datadog-vs-gitlab.html)

* [New Relic](https://about.gitlab.com/devops-tools/new-relic-vs-gitlab.html)

* [Micro Focus APM](https://about.gitlab.com/devops-tools/microfocus-APM-vs-gitlab.html)






