---
layout: job_family_page
title: "Recruiting Director"
---

The Director of Recruiting is responsible for supporting the execution of the overall strategy for the Recruiting team. The Director of Recruiting reports to the VP of Recruiting and works closely with the organizational leaders in shaping GitLab's recruiting strategies to support hiring targets, innovation and rapid growth. The Director of Recruiting uses their experience in Recruiting areas of organizational strategy, workforce planning, candidate experiences, and compliance with hiring policies and procedures.

## Senior Director 

## Responsibilities

* Supports the execution of the overall strategy of the Recruiting team 
* Drives the execution of our geographical dispersion talent plan 
* Partners with the Director and the VP of recruiting to align on overall plan for talent acquisition to ensure we maintain the high caliber of team members 
* Provides support to the VP of Recruiting in delivering data lead workforce planning processes
* Lead selection/management of vendor relationships globally. Negotiate costs to stay within budget and maximize ROI
* Provides guidance on how to operationalize the diversity efforts within GitLab to drive a diverse talent pool for our open roles 
* Provide leadership to the Global Recruiting team by creating a culture of accountability with a focus on delivering measurable results 
* Partnership with the VP of Recruiting on the structure and capabilities required of the Recruiting team 
* Hands-on Leadership of executive-level searches 
* Drive an engaging team culture based on GItLab's core values

## Requirements
* Proven experience in high growth environment
* Successfully driven globally distributed hiring
* Excellent people management skills, enabling the Recruiting team to reach their full potential
* Led a high performing Recruiting team
* Strong attention to detail and ability to work well with changing information
* Comfortable using Technology including a working knowledge of GitLab
* Succinct communication skills with the ability to collaborate with cross functional team members
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem solving
* Have implemented various HRIS/ATS systems or recruiting tools while delivering internal OKRs
* 10+ years recruiting experience with a minimum of 2 years managing a recruiting team.
* Experience hiring Global Talent in areas like EMEA and APAC

## Director

## Responsibilities

* Provide strategic input on the direction of the recruiting team to the VP of Recruiting
* Develops new methods to find passive talent and talent from low cost areas across the world.
* Partner with the leaders of the organization to ensure that they are planning for the right hires and are invested in the process of finding, evaluating, and onboarding their talent.
* Identify and Operationalize the technology needs of the recruiting team - identifying needs, determining solutions and integrating tools and systems
* Partner with internal and external teams to deep dive on trends, opportunities and build projects to continuously improve the effectiveness of finding the best talent for GitLab
* Actively research and source for high priority, executive positions across the organization.
* Evangelize a leading candidate experience from the initial recruiter interview to candidate onboarding
* Manage the recruitment team workload, evaluate and assign priorities to internal recruiting team emphasizing ownership and accountability
* Provide insight to optimize processes and technology
* Build out GitLab talent programs including diversity recruiting, college/campus recruiting, internships and apprenticeships.
* Utilize Greenhouse ATS to define recruiting metrics and using data to implement improvements for talent acquisition
* Cultivate strong partnerships with hiring teams and executives, and influence change throughout the entire hiring process
* Training for hiring managers and team members on GitLab hiring practices. Also influence those hire practices as they may need to be iterated on to drive the best, scalable results for GitLab.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

## Requirements

* Strong attention to detail and ability to work well with changing information
* Comfortable using Technology including a working knowledge of GitLab
* Succinct communication skills with the ability to collaborate with cross functional team members
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem solving
* Have implemented various HRIS/ATS systems or recruiting tools while delivering internal OKRs
* 5-10+ years recruiting experience with a minimum of 2 years managing a recruiting team.
* Experience hiring Global Talent in areas like EMEA and APAC

## Performance Indicators

*   [Hires vs. Plan](https://about.gitlab.com/handbook/hiring/metrics/#hires-vs-plan)
*   [Apply to Offer Accept](https://about.gitlab.com/handbook/hiring/metrics/#apply-to-offer-accept-days)
*   [Offer Acceptance Rate](https://about.gitlab.com/handbook/hiring/metrics/#offer-acceptance-rate)
*   [Candidate ISAT](https://about.gitlab.com/handbook/hiring/metrics/#interviewee-satisfaction-isat)
*   [Average Location Factor]( https://about.gitlab.com/handbook/people-operations/people-operations-metrics/#average-location-factor)
*   [Candidates Sourced vs. Candidates Hired](https://about.gitlab.com/handbook/hiring/metrics/#candidates-sourced-vs-candidates-hired)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/).

   * Qualified candidates will be invited to schedule a 30 minute screening call with a GitLab Recruiter
   * Then, candidates will be invited to schedule two separate 50 minute interviews; a Recruiting Manager, and the Director, Global People Operations
   * Next, candidates will be invited to schedule 4 separate 50 minute interviews; VP of Field Operations, VP of Engineering, Chief Legal Officer, and the Chief Financial Officer
   * Finally, candidates will be invited to a 50 minute interview with the CEO

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).

