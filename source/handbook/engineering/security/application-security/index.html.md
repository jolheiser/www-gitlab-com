---
layout: markdown_page
title: "Application Security"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Application Security Mission

The application security team works closely with engineering and product teams
to ensure that all GitLab products securely handle the customer data with
which we are entrusted.

## Role Functions

### Stable Counterparts

The overall goal of application security stable counterparts is to help
integrate security themes early in the development process. This is with
the goal of reducing the number of vulnerabilities released over the long
term.

#### Technical Goals
- Assist in threat modeling features to identify areas of risk prior to
  implementation
- Code and app sec reviews
- Provide guidance on security best practices
- Improve security testing coverage
- Assistance in prioritizing security fixes

#### Non-technical Goals
- Enable development team to self-identify security risk early
- Help document and solve pain points when it comes to security process
- Identify vulnerability areas to target for training and/or automation
- Assist in cross-team communication of security related topics
