---
layout: markdown_page
title: "Sales Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}


### HOW TO COMMUNICATE WITH US

Slack: [#sales-support](https://gitlab.slack.com/messages/CNLBL40H4)  
SFDC: [@sales-Support](https://gitlab.my.salesforce.com/_ui/core/chatter/groups/GroupProfilePage?g=0F94M000000fy2K)

### CHARTER

Sales Operations is responsible for helping make sales more effective and efficient. We aim to help facilitate new and existing processes throughout our field organization via the use of systems, policies and direct support. Our internal goal is to automate and streamline as much as possible to allow us to get more done, but where there is no automation, we aim to provide direct, white-glove support to our sales organization.

Sales Operations is a part of (Field Operations), and is responsible for the following key areas:

*  Territories
*  Go To Market data
*  Deal Support (see Deal Desk)
*  Sales Support
*  Compensation
*  Partner Operations
*  Customer Success Operations

### Sales Support - Deal Desk and Sales Operations

The GitLab Sales Support team includes two groups: Deal Desk and Sales Operations.

The Deal Desk team is comprised of in-field resources aligned to the time zones of our sales team.
*  East and West coast analysts for Americas and APAC support
*  EMEA analysts for EMEA and APAC support

The Sales Support team is a service function responsible for providing guidance to the sales team in any queries that may arise at opportunity and account level, providing quoting assistance, when required, quote review and approval of opportunities. Also, the team may help on technical issues you might be having. 

Reaching the Sales Support Team (Internally)

Chatter @sales-support on the specific record in Salesforce you need quote assistance, calculations of Incremental ACV, technical issues, or other problems you are having. 

The Slack Chat used by this team is #sales-support and should be used only if you have general questions, not related to a specific record in Salesforce.

Please avoid contacting individual directly so that the discussion can be available for everyone to review.

SLAs:
First touch within 4 business hours
Resolution within 24 business hours

Typical inquiries to Sales Support may include:
*  Pending Quote Approvals
*  Deal Desk Pending Approvals (opportunities to review or approve as Closed Won)
*  Quote generation
*  Ramp deals
*  Contract resets (credit opportunity and upgrade/add-on opp)
*  Quote assistance on existing quote objects
*  IACV calculation
*  Renewal ACV calculation
*  Vendor set-up requests
*  Salesforce Reporting
*  Salesforce Dashboards
*  Questions regarding territory alignment and ownership
*  Billing questions or support
*  Legal questions or support

### Flat Renewal Support

The Deal Desk supports processing of flat renewals which meet the following parameters:
* Renewal ACV is under $10,000
* Flat renewal - no upside or downside IACV (same value year over year)
* Renewal has no true-up
* Customer is in good standing (no billing disputes or collection issues)
* Billing contact and address is up to date
* No customer interaction is required

How to request flat renewal support:

Renewal opportunities have a "Flat Renewal Support" button. When this feature is used, a case will be auto-created for the Deal Desk team to execute the renewal. The Deal Desk team will make sure renewal meets the above criteria before moving forward with the renewal.

### Quote Templates 

In order to add new or deprecate quote templates the relevant process' should be followed
*  New quote templates should always bee coordinated with the legal team 
*  For adding new quote templates follow the instructions provided by Zuora to [customize the template](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates) and how to [upload them for use in Salesforce](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings)
*  In order to deprecate a quote template in Salesforce contact a System Administrator mark the quote template field of `Hide Template?` to TRUE in Saesforce
