---
layout: markdown_page
title: "Sales & Customer Enablement"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Welcome to the Sales & Customer Enablement Handbook

**CHARTER**

Ensure everyone in the GitLab sales, partner, and customer ecosystem has the right skills, knowledge, and resources to meet and exceed desired business objectives in accordance with Gitlab’s values.

**KEY PROGRAMS**

*  [Sales Onboarding](/handbook/sales/onboarding/)
*  [GitLab Command of the Message](/handbook/marketing/product-marketing/sales-resources/#command-of-the-message)
*  [Continuous Improvement / Continuous Development for Sales (CI/CD for Sales)](/handbook/sales/training/)
