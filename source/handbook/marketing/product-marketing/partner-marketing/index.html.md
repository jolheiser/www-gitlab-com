---
layout: markdown_page
title: "Partner Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Partner marketing at GitLab

- Promote existing partnerships to be at top-of-mind for developers.
- Integrate resale partnerships: promote partnership integrations/products which we sell as part of our sales process.
- Migrate open source projects to adopt GitLab, and convert their users in-turn to GitLab.
- Surface the ease of GitLab integration to encourage more companies to integrate with GitLab.
- Build closer relationships with existing partners through consistent communication

For a list of Strategic Partners, search for "Partnerships" on the Google Drive.

##### Partner marketing activation

Our partner activation framework consists of a series of action items within a high-level issue. When a strategic partnership is signed, Product Marketing will choose the issue template called [partner-activation](https://gitlab.com/gitlab-com/marketing/general/issues/new) which will trigger notification for all involved in partner activation activities.

For each action item within this issue, a separate issue should be created by the assignee who is responsible for that action item within the allocated timeframe.

The partner should be included on the high level issue so they can see the planned activities and can contribute.

##### Partner newsletter feature

In line with the objective of "Promote existing partnerships to be at top-of-mind for developers", a regular feature in our fortnightly (8th & 22nd) newsletter will promote our partners to our target audience. This feature should be co-authored by the partner.

Possible content:

- Feature on new partner if signed
- Feature on existing partner if major update released
- Feature on existing partner highlighting benefits of partner product
- 1-minute video showcasing the integration as a reference
- Blog post from partner
- Feature on how existing customer uses GitLab and partner

Suggested format:

- Length: Couple of paragraphs
- Links: GitLab integration/partner page & partner website

Creation:

Email potential partner with case for creating content/blog post which will feature in our newsletter. Also request that they include the content in their own newsletter.

Create separate issue for blog post in www-gitlab-com project with blog post label and assign to Content Marketing with the following information:
- A summary describing the partnership
- Any specific goals for the blog post (e.g. must link to this, mention this, do not do this...)
- Contacts from both sides for people involved in the partnership
- Any marketing pages or material created from both sides which we can use in the post
- Deadlines for the post (internal draft, partner draft review and publication)

After publication: Send to partner a summary of the click-throughs to their website, registration for webinar etc.

- View the [Partner Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/general/-/boards/814970) to see what's currently in progress.

### Which partner marketing manager should I contact?

- Listed below are areas of responsibility within the partner marketing team:

  - [Tina](/company/team/#t_sturgis), Manager, Partner and Channel Marketing
  - [Ashish](/company/team/#kuthiala), Director PMM
