---
layout: markdown_page
title: "Education Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

- GitLab's top tiers (self-hosted Ultimate and cloud-hosted Gold) are [free for education](/solutions/education/)

## Requirements

- Any institution whose purposes directly relates to learning, teaching, and/or training may qualify. Educational purposes do not include commercial, professional, or any other for-profit purposes. The education license is only for the purposes of educating students and can be used by students and the teachers providing the education.
- See our [full terms](/terms/#edu-oss) regarding this program
- For more examples, please see the [FAQ section](/solutions/education/#FAQ)

## Workflows

- Community Advocacy [workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/)

## Metrics

SFDC Opportunities by Stage for EDU campaign - SFDC [report](https://na34.salesforce.com/00O61000004hfnt)
